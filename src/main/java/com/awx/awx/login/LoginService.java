package com.awx.awx.login;

import com.awx.awx.User.User;
import com.awx.awx.session.Session;
import com.awx.awx.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Service
public class LoginService {

    @Autowired
    private SessionRepository sessionRepository;

    public void login(User user, HttpServletResponse response) {
        Session s = new Session(user);
        sessionRepository.save(s);

        Cookie cookie = new Cookie("sessionId", s.getId());
        response.addCookie(cookie);
    }
}
