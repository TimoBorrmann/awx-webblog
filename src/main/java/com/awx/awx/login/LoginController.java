package com.awx.awx.login;

import com.awx.awx.User.User;
import com.awx.awx.User.UserRepository;
import com.awx.awx.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class LoginController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    private LoginService loginService;

    @GetMapping("login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("login")
    public String loginSubmit(Model model, LoginDTO login, HttpServletResponse response) {
        Optional<User> optionalUser = userRepository.findFirstByNameAndPassword(login.getUsername(), login.getPassword());
        if (optionalUser.isPresent()) {
            loginService.login(optionalUser.get(), response);
            return "redirect:/";
        }
        model.addAttribute("login", login);
        return "home";
    }

    @PostMapping("logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        sessionRepository.deleteById(sessionId);
        Cookie c = new Cookie("sessionId", "");
        c.setMaxAge(0);
        response.addCookie(c);
        return "logout";
    }
}
