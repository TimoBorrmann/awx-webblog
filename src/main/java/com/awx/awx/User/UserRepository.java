package com.awx.awx.User;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository <User, Long>{

    Optional<User> findFirstByNameAndPassword(String name, String password);

    boolean existsByName(String name);

    Optional<User> findById(Long id);

    List<User>findAllByAdminFalse();
}
