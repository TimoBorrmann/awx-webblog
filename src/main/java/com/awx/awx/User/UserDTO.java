package com.awx.awx.User;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

public class UserDTO {


    @NotEmpty(message = "Bitte Benutzername eingeben")
    @Column(unique = true)
    private String name;

    private String password;

    private boolean admin = false;



    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
