package com.awx.awx.User;

import com.awx.awx.Beitrag.Beitrag;
import com.awx.awx.Beitrag.BeitragDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/makeAdmin/{id}")
    public String userZuAdmin(@PathVariable (name="id") Long id) {
        User selectedUser = userRepository.findById(id).get();
        userService.ernenneUserzuAdmin(selectedUser);

        return "redirect:/";
    }

}