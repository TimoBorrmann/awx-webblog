package com.awx.awx;

import com.awx.awx.Beitrag.Beitrag;
import com.awx.awx.Beitrag.BeitragController;
import com.awx.awx.Beitrag.BeitragRepository;

import com.awx.awx.Beitrag.BeitragService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringBootApplication
public class AwxMain {

	@Autowired
	BeitragService beitragService;
	@Autowired
	BeitragRepository beitragRepository;

	public static void main(String[] args) {
		SpringApplication.run(AwxMain.class, args);
	}

//	@PostConstruct
//	public void initData() {
//		beitragService.initData();
//	}
//		@PostConstruct
//		public void sortiereBeitraege(){
//
//		List<Beitrag> beitragList = beitragRepository.findAllByOrderByIdDesc(); //o. auch beitragController
//		for (Beitrag beitrag : beitragList) {
//			System.out.println(beitrag.getCreationDate() + " "+ beitrag.getTitle());
//		}
//	}


}
