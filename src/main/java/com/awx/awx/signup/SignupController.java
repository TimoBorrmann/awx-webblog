package com.awx.awx.signup;

import com.awx.awx.User.User;
import com.awx.awx.User.UserRepository;
import com.awx.awx.login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class SignupController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    LoginService loginService;

    @GetMapping("signup")
    public String signup(Model model) {
        model.addAttribute("signup", new SignupDTO());
        return "signup";
    }

    @PostMapping("signup")
    public String signup(@ModelAttribute("signup") @Valid SignupDTO signup, BindingResult bindingResult, HttpServletResponse response) {
        if (!signup.getPassword1().equals(signup.getPassword2())) {
            bindingResult.addError(new FieldError("signup", "password2", "Passwords do not match"));
        }

        if (userRepository.existsByName(signup.getUsername())) {
            bindingResult.addError(new FieldError("signup", "username", "Username taken"));
        }

        if (bindingResult.hasErrors()) {
            return "signup";
        }

        User user = new User(signup.getUsername(), signup.getPassword1());
        userRepository.save(user);

        loginService.login(user, response);

        return "redirect:/";
    }
}
