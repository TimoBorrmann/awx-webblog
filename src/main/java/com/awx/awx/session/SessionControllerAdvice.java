package com.awx.awx.session;


import com.awx.awx.User.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {

    @Autowired
    SessionRepository sessionRepository;

    @ModelAttribute("currentUser")
    public User currentUser(@CookieValue(value = "sessionId", defaultValue = "") String sessionId) {
        if (sessionId.length() > 0) {
            Optional<Session> sess = sessionRepository.findById(sessionId);
            if (sess.isPresent()) {
                return sess.get().getUser();
            }
        }
        return null;
    }
}
