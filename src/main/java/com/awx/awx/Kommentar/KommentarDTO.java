package com.awx.awx.Kommentar;

import com.awx.awx.User.User;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.Instant;

public class KommentarDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id=0;

    private Instant creationDate;

    private String kommentarTitle="";

    private User user;

    public KommentarDTO() {
    }

    public KommentarDTO(String kommentarTitle,Instant creationDate, User user) {
        this.creationDate = Instant.now();
        this.kommentarTitle = kommentarTitle;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getKommentarTitle() {
        return kommentarTitle;
    }

    public void setKommentarTitle(String kommentarTitle) {
        this.kommentarTitle = kommentarTitle;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
