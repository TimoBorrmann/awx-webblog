package com.awx.awx.Kommentar;

import com.awx.awx.Beitrag.Beitrag;
import com.awx.awx.Beitrag.BeitragRepository;
import com.awx.awx.User.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class KommentarController {
    @Autowired
    KommentarRepository kommentarRepository;
    @Autowired
    BeitragRepository beitragRepository;


    @PostMapping("/kommentar")
    public String createKommentar(@ModelAttribute("kommentar") @Valid KommentarDTO kommentarDTO, BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser, @RequestParam("beitragId") long id) {
        if(bindingResult.hasErrors()){
            return "redirect:/";
        }
        Kommentar kommentar = new Kommentar(kommentarDTO.getKommentarTitle(), kommentarDTO.getCreationDate(), currentUser, beitragRepository.findById(id).get());
        kommentarRepository.save(kommentar);

        return "redirect:/details?beitragId="+id;
    }


}
