package com.awx.awx.Kommentar;

import com.awx.awx.Beitrag.Beitrag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KommentarRepository extends JpaRepository <Kommentar, Long> {
    List<Kommentar> findAllByOrderByCreationDateDesc();
//    void deleteById(long id);

}
