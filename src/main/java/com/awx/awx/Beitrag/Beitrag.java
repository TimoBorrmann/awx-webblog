package com.awx.awx.Beitrag;

import com.awx.awx.Kommentar.Kommentar;
import com.awx.awx.User.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Instant creationDate;


    @NotEmpty
    @Lob
    private String text;
    @NotEmpty
    @Lob
    private String title;

    @OneToMany(mappedBy = "beitrag", cascade = CascadeType.ALL)
    @OrderBy
    private List <Kommentar> kommentarList = new ArrayList<>();

    @ManyToOne
    private User user;

    public Beitrag(){

    }

    public Beitrag( String text, String title, User user) {

        this.creationDate = Instant.now();
        this.text = text;
        this.title = title;
        this.user = user;
    }


    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public List<Kommentar> getKommentarList() {
        return kommentarList;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setKommentarList(List<Kommentar> kommentarList) {
        this.kommentarList = kommentarList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
